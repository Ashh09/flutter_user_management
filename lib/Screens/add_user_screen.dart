import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_user_management/utils/api.dart';
import 'package:flutter_user_management/widgets/app_drawer.dart';


class AddUserScreen extends StatefulWidget {
    @override
    AddUserScreenState createState() => AddUserScreenState();
}
class AddUserScreenState extends State<AddUserScreen> {
    
    Future? _futureUser;
    
    final _formKey = GlobalKey<FormState>();
    final _txtNameController = TextEditingController();
    final _txtEmailController = TextEditingController();
    final _txtPhoneController = TextEditingController();
    final _txtWebsiteController = TextEditingController();


    void addUser(BuildContext context) {
        API().addUser(
            name: _txtNameController.text, 
            email: _txtEmailController.text,
            phone: _txtPhoneController.text,
            website: _txtWebsiteController.text
        ).catchError((error) {
            showSnackBar(context, error.message);
        });
    }

    void showSnackBar(BuildContext context, String message){
            SnackBar snackBar = new SnackBar(
                content: Text(message),
                duration: Duration(milliseconds: 2000)
            );
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
        }


    @override
    Widget build(BuildContext context) {

         Widget txtName = TextFormField(
            decoration: InputDecoration(labelText: 'Name'),
            controller: _txtNameController,
            validator : (name){ 
                if (name == null || name.isEmpty){
                    return "The name must be provided.";
                }

            }
        );


         Widget txtEmail = TextFormField(
            decoration: InputDecoration(labelText: 'Email'),
            keyboardType: TextInputType.emailAddress,
            controller: _txtEmailController,
            validator : (email){ 
                return (email != null && email.isNotEmpty && email.contains('@')) ? null : 'Invalid email.';

            }
        );

        Widget txtPhone = TextFormField(
            decoration: InputDecoration(labelText: 'Phone number'),
            controller: _txtPhoneController,
            validator : (phone){ 
                if (phone == null || phone.isEmpty){
                    return "A phone number must be provided.";
                }

            }
        );

        Widget txtWebsite = TextFormField(
            decoration: InputDecoration(labelText: 'Website'),
            controller: _txtWebsiteController,
            validator : (website){ 
                if (website == null || website.isEmpty){
                    return "A website  must be provided.";
                }

            }
        );

        Widget btnAdd = Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 8.0),
            child: ElevatedButton(
                child: Text('Add User'),
                onPressed: () {
                    if(_formKey.currentState!.validate()){
                        addUser(context);
                         Navigator.pushNamedAndRemoveUntil(context, '/', (route) => false);
                        showSnackBar(context, 'User Added');
                    }else{
                        showSnackBar(context, "Update fields to pass all validations.");
                    }
                }
            )
        );


        Widget formRegister = SingleChildScrollView(
            child: Form(
                key: _formKey,
                child: Column(
                    children: [
                        txtEmail,
                        txtName,
                        txtPhone,
                        txtWebsite,
                        btnAdd
                    
                    ]
                )
            )
        );

        Widget registerView = FutureBuilder(
            future: _futureUser,
            builder: (context, snapshot) { 
                if (_futureUser == null) {
                    return formRegister;
                } else if (snapshot.hasData && snapshot.data == true){
                    Timer(Duration(seconds: 3),(){
                        Navigator.pushNamedAndRemoveUntil(context, '/', (route) => false);
                    });
                    return Column (
                        children: [
                            Text('Registration successful. You will be redirected shortly to the login page .'),
                        ],
                    );
                } else {  
                    return Center(  
                        child: CircularProgressIndicator(),
                    );  
                }
            }
        );
        return Scaffold(
           appBar: AppBar(title: Text('Add User'),
            leading: IconButton(
                        icon: Icon(Icons.arrow_back, color: Colors.black),
                        onPressed: () => Navigator.of(context).pop(),
                     ),
            ),
            endDrawer: AppDrawer(),
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.all(16.0),
                child: registerView
            ),
        );
    }
}