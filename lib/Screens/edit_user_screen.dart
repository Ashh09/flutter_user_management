import 'dart:async';
import 'package:flutter_user_management/models/user.dart';

import '/utils/api.dart';

import 'package:flutter/material.dart';
import 'package:flutter_user_management/utils/api.dart';
import 'package:flutter_user_management/widgets/app_drawer.dart';


class EditUserScreen extends StatefulWidget {

    final User?  user;

    EditUserScreen( this.user );
    @override
    _EditUserScreenState createState() => _EditUserScreenState();

    
}
class _EditUserScreenState extends State<EditUserScreen> {
    
    Future? _futureUser;
    
    final _formKey = GlobalKey<FormState>();
    final _txtNameController = TextEditingController();
    final _txtEmailController = TextEditingController();
    final _txtPhoneController = TextEditingController();
    final _txtWebsiteController = TextEditingController();
    
    
   


    void editUser(BuildContext context){
        setState((){
            _futureUser = API().editUser(
                
                name: _txtNameController.text, 
                email: _txtEmailController.text,
                phone: _txtPhoneController.text,
                website: _txtWebsiteController.text, 
                id: widget.user!.id,

            ).catchError((error){
                showSnackBar(context, error.toString());
            });
        });
    }
    void showSnackBar(BuildContext context, String message){
            SnackBar snackBar = new SnackBar(
                content: Text(message),
                duration: Duration(milliseconds: 2000)
            );
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
        }


    @override
    Widget build(BuildContext context) {

         Widget txtName = TextFormField(
            decoration: InputDecoration(labelText: 'Name'),
            controller: _txtNameController,
            validator : (name){ 
                if (name == null || name.isEmpty){
                    return "The name must be provided.";
                }

            }
        );


         Widget txtEmail = TextFormField(
            decoration: InputDecoration(labelText: 'Email'),
            keyboardType: TextInputType.emailAddress,
            controller: _txtEmailController,
            validator : (email){ 
                return (email != null && email.isNotEmpty && email.contains('@')) ? null : 'Invalid email.';

            }
        );

        Widget txtPhone = TextFormField(
            decoration: InputDecoration(labelText: 'Phone number'),
            controller: _txtPhoneController,
            validator : (phone){ 
                if (phone == null || phone.isEmpty){
                    return "A phone number must be provided.";
                }

            }
        );

        Widget txtWebsite = TextFormField(
            decoration: InputDecoration(labelText: 'Website'),
            controller: _txtWebsiteController,
            validator : (website){ 
                if (website == null || website.isEmpty){
                    return "A website  must be provided.";
                }

            }
        );

        Widget btnEdit = Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 8.0),
            child: ElevatedButton(
                child: Text('Edit User'),
                onPressed: () {
                    if(_formKey.currentState!.validate()){
                        editUser(context);
                         Navigator.pushNamedAndRemoveUntil(context, '/', (route) => false);
                        showSnackBar(context, 'Successfully edited');
                    }else{
                        showSnackBar(context, "Update registration fields to pass all validations.");
                    }
                }
            )
        );


        Widget formRegister = SingleChildScrollView(
            child: Form(
                key: _formKey,
                child: Column(
                    children: [
                        txtEmail,
                        txtName,
                        txtPhone,
                        txtWebsite,
                        btnEdit
                    
                    ]
                )
            )
        );

        Widget editView = FutureBuilder(
                    future: _futureUser,
                    builder: (context, snapshot) { 
                        if (_futureUser == null) {
                            return formRegister;
                        } else if (snapshot.hasData && snapshot.data == true){
                            Timer(Duration(seconds: 3),(){
                                Navigator.pushNamedAndRemoveUntil(context, '/', (route) => false);
                            });
                            return Column (
                                children: [
                                    Text('Edited Successfully.'),
                                ],
                            );
                        } else {  
                            return Center(
                                child: CircularProgressIndicator(),
                            );  
                        }
                    }
                );
        return Scaffold(
            appBar: AppBar(title: Text('Edit User'),
            leading: IconButton(
                        icon: Icon(Icons.arrow_back, color: Colors.black),
                        onPressed: () => Navigator.of(context).pop(),
                     ),
            ),
            endDrawer: AppDrawer(),
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.all(16.0),
                child: editView
            ),
        );
    }
}