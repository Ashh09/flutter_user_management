import 'package:flutter/material.dart';

class AppDrawer extends StatelessWidget {
    @override
    Widget build(BuildContext context) {

        return Drawer(
                child: ListView(
                    children: [
                        ListTile(
                            title: Text('Add User'),
                            onTap: ()  {
                                Navigator.pushNamed(context, '/add-user');
                            }
                        )
                    ]
                )
            );
    }
}