import 'package:flutter/material.dart';
import 'package:flutter_user_management/models/user.dart';

import '/screens/user_list_screen.dart';
import '/screens/add_user_screen.dart';
import 'Screens/edit_user_screen.dart';

void main() {
    runApp(App());
}

class App extends StatelessWidget {
  User? user;

    @override
    Widget build(BuildContext context) {
        return MaterialApp(
            theme: ThemeData(
                    primaryColor: Color.fromRGBO(203, 22, 11, 1),
                    elevatedButtonTheme: ElevatedButtonThemeData(
                        style: ElevatedButton.styleFrom(
                            primary: Color.fromRGBO(203, 22, 11, 1), //background color
                            onPrimary: Colors.black //Text color
                        )
                    )

                ),
            initialRoute: '/',
            routes: {
                '/': (context) => UserListScreen(),
                '/add-user': (context) => AddUserScreen(),
                '/edit-user': (context) => EditUserScreen(user),
            }
        );
    }
}